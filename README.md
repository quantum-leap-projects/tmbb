# Temperature Measurement with BLE Beacons

Measuring and evaluating temperature and humidity data with BLE beacons. 

## Hardware:
I tried to have a wireless setup, for more flexibility.

## Gateway
- tested on a Raspberry Pi Zero W and a Raspberry Pi 4 Model B. 
- OS: Raspbian OS or Ubuntu Server

Notice: The setup uses the gittlab library with python, the build process requires roughly 1GB of RAM. When using the Pi Zero increase the size of you SWAP-Memory to successfully pip-install the library.  

## BLE Beacon 

Temperature and Humidity Sensor Light is a new generation sensor technology developed by JINOU Company based on iBeacon. This product can send collected data to phone or gateway via Beacon Broadcasting.
Specification:
- working frequency: ISM frequency 2.400 ~ 2.483 GHz.
- Bluetooth specification: BLE5.0.
- spread spectrum method: FHSS
- voltage: 2.7 - 3.3 V.
- transmission power: 0dBm
- transmission range: 50 m.
- receive sensitivity: <-96dBm at < 0.1% BER.
- storage temperature: -55 °C ~ 125 °C.
- standby power: approx. 0.2 mA.
- antenna standard: 2.4 G, 50 Ohm.
- security standard: AES 128bit
- temperature measuring range: -40 °C ~ +85 °C.
- temperature measurement accuracy: between 15 °C ~ +40 °C, deviation is ± 0.5 °C. Between 40 °C ~ +85 °C. Deviation is ± 1 °C.
- relative humidity measuring range: 0 ~ 100 %.
- humidity measurement accuracy: between 20 + 80% RH, deviation is ± 4.5%. 
