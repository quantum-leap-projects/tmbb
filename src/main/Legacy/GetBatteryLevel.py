#!/usr/bin/python3
# run as standalone process to get temperature data from ble
import sys
import BLEConnectionHandler
from csv import writer
import datetime
#TODO: Add log for errors.

def append_list_as_row(file_name, list_of_elem):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)

class Battery(object):
    # default uuid "0000aa21-0000-1000-8000-00805f9b34fb"
    uuid = None
    address = None
    now = datetime.datetime.now()
    now = str(now).split()

    def __init__(self, address: str, uuid: str):
        self.uuid = uuid
        self.address = address
        self.requester = BLEConnectionHandler.BLEConnectionHandler(address, uuid)

    def request_battery(self):
        try:
            data = self.requester.request_data()
            data = self.parse_bytes_to_battery_level(data)
            return data
        except Exception as e:
            print(e)

    def parse_bytes_to_battery_level(self, data):
        # data element nr. 4 is irrelevant
        print(data)
        return data


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: {} <addr>".format(sys.argv[0]))
        sys.exit(1)
    address = sys.argv[1]

    #uuid = "0000aa21-0000-1000-8000-00805f9b34fb"
    uuid = sys.argv[2]
    file_name = 'th-measurements.csv'

    readTemp = Battery(address, uuid)
    temperature = readTemp.request_battery()
    # if temperature is None:
    #     temperature = ['', '']
    # data = [address] + readTemp.now + temperature
    # print(data)
    # #append_list_as_row(file_name, data)
    # time.sleep(2)
    # #print("Done.")
