import pygatt

adapter = pygatt.GATTToolBackend()

try:
    adapter.start()
    device = adapter.connect('DC:6C:7A:4D:C3:8C')
    value = device.char_read("0000aa21-0000-1000-8000-00805f9b34fb")
    print(value)

except pygatt.exceptions.NotConnectedError as e:
    print("")

finally:

    adapter.stop()
