import datetime
import logging
import os.path
import pathlib
import subprocess
import time
from csv import writer

import yaml

with open('resource/config.yml', 'r') as file:
    ble_config = yaml.safe_load(file)

logging.basicConfig(filename="temp.log", level=logging.DEBUG)


def append_list_as_row(file_name, list_of_elem: list, noEmptyValues):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        if '' not in list_of_elem:
            csv_writer.writerow(list_of_elem)


def request_temp(ble_config, ble):
    ble_address = ble_config["address"][ble]
    temperature_uuid = ble_config["uuid"]["temp"]
    logging.info('Accessing Beacon ' + ble_address)

    get_temp_path: pathlib.Path = os.path.dirname(
        __file__) + os.path.sep + "ble_connector" + os.path.sep + "GetTempData.py"

    temperature_level = subprocess.run(["python3", get_temp_path, ble_address, temperature_uuid], capture_output=True,
                                       text=True)
    logging.info(temperature_level.stdout)
    return temperature_level.stdout


def request_battery(ble_config, ble):
    ble_address = ble_config["address"][ble]
    battery_uuid = ble_config["uuid"]["battery"]
    print(ble_address)
    get_bat_path: pathlib.Path = os.path.dirname(
        __file__) + os.path.sep + "ble_connector" + os.path.sep + "GetBatteryLevel.py"

    battery_status = subprocess.run(["python3", get_bat_path, ble_address, battery_uuid], capture_output=True,
                                    text=True)
    return battery_status.stdout


if __name__ == '__main__':
    while True:
        now = datetime.datetime.now()
        now = str(now).split()
        file_name = 'th-measurements.csv'

        for ble in ble_config["address"]:
            temperature = None

            try:
                temperature = request_temp(ble_config, ble)
            except Exception as e:
                logging.error(e)
            finally:
                # handle empty temp readings.
                if temperature is None:
                    temperature = ['', '']
                data = [ble, now, temperature]
                append_list_as_row(file_name, data, noEmptyValues=True)
                time.sleep(1)
        time.sleep(ble_config["time_frame"]["sec"])
