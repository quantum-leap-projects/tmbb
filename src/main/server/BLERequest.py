from connection.AbstractBleConnectionHandler import AbstractBleConnectionHandler
from response.AbstractBleResponse import AbstractBleResponse
from util.Ensure import Ensure


class BleRequest(object):
    connection_handler: AbstractBleConnectionHandler = None
    ble_response_format: AbstractBleResponse = None

    def __init__(self, ble_handler: AbstractBleConnectionHandler, ble_response_format: AbstractBleResponse):
        Ensure.not_none(ble_handler)
        Ensure.not_none(ble_response_format)
        self.connection_handler = ble_handler
        self.ble_response_format = ble_response_format

    def request(self):
        raw_result = self.connection_handler.request_data()
        if raw_result is not None:
            return self.ble_response_format.format_response(raw_result)
