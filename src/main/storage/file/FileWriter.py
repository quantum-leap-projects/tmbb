import pathlib
from csv import writer

from storage.BleBeacon import BleBeacon
from util.Ensure import Ensure
import datetime as dt

class FileWriter:
    BLANK = ''

    file_name: pathlib = None

    def __init__(self, file_name: str):
        Ensure.string_not_blank(file_name)
        self.file_name = file_name

    def append_file(self, beacon:BleBeacon, elements):
        with open(self.file_name, 'a+', newline=self.BLANK) as write_obj:
            csv_writer = writer(write_obj)
            if self.BLANK not in elements:
                line_to_write = [dt.datetime.now().strftime('%d/%m/%y %I:%M %S %p'), beacon.mac, beacon.uuid]
                for key in elements.keys():
                        line_to_write.append(elements.get(key))
                csv_writer.writerow(line_to_write)
