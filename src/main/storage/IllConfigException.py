class EmptyConfigException(Exception):
    """Exception raised for empty configuration files.

        Attributes:
            file_path -- input config
            message -- explanation of the error
        """
    def __init__(self, file_path, message="Config file was empty."):
        self.file_path = file_path
        self.message = message
        super().__init__(self.message)


class MissingBleAddressException(Exception):
    """Exception raised for missing ble mac addresses in configuration files.

        Attributes:
            file_path -- input config
            message -- explanation of the error
        """
    def __init__(self, file_path, message='Missing ble address in config or typo for key "address".'):
        self.file_path = file_path
        self.message = message
        super().__init__(self.message)


class MissingUuidException(Exception):
    """Exception raised for missing uuids in configuration files.

        Attributes:
            file_path -- input config
            message -- explanation of the error
        """
    def __init__(self, file_path, message='Missing uuids in config or typo for key "uuid".'):
        self.file_path = file_path
        self.message = message
        super().__init__(self.message)


class EmptyFileNameException:
    """Exception raised for missing file in configuration files.

           Attributes:
               file_path -- input config
               message -- explanation of the error
           """

    def __init__(self, file_path, message='Missing file path in config or typo for key "file".'):
        self.file_path = file_path
        self.message = message
        super().__init__(self.message)