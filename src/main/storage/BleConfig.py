import yaml

from storage import IllConfigException


class BleConfig:

    def reload_config(self):
        pass

    def load_config(self, config_file) -> dict:
        with open(config_file, 'r') as file:
            ble_config: dict = yaml.safe_load(file)
        if ble_config is None:
            raise IllConfigException.EmptyConfigException(config_file)

        if 'address' not in ble_config:
            raise IllConfigException.MissingBleAddressException(config_file)

        if 'uuid' not in ble_config:
            raise IllConfigException.MissingUuidException(config_file)

        return ble_config
