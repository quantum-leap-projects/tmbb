class BleBeacon:
    mac: str
    uuid: str

    def __init__(self, mac: str, uuid: str):
        self.mac = mac
        self.uuid = uuid