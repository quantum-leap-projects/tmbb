class NotNoneException(Exception):
    """Raised for missing data/None-Type in parameters.

        Attributes:
            message -- explanation of the error
        """

    def __init__(self, message='Parameter may not be none.'):
        self.message = message
        super().__init__(self.message)


class NotEmptyListException(Exception):
    """Raised for empty list in parameters.

        Attributes:
            message -- explanation of the error
        """

    def __init__(self, message='List may not be empty.'):
        self.message = message
        super().__init__(self.message)


class BlankStringException(Exception):
    """Raised for blank strings (none, blank, white space).
        Attributes:
            message -- explanation of the error
        """

    def __init__(self, message='String may not be none, blank or white space.'):
        self.message = message
        super().__init__(self.message)
