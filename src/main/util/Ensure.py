from util import EnsureException


class Ensure(object):
    @classmethod
    def not_none(cls, data):
        if data is None:
            raise EnsureException.NotNoneException

    @classmethod
    def not_empty_list(cls, data: list):
        if not data:
            raise EnsureException.NotEmptyListException

    @classmethod
    def string_not_blank(cls, data: str):
        if data is None:
            raise EnsureException.BlankStringException
        if len(data) is 0:
            raise EnsureException.BlankStringException
        if data.isspace():
            raise EnsureException.BlankStringException
