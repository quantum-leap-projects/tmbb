import logging
import sys

from connection.AbstractBleConnectionHandler import AbstractBleConnectionHandler
from storage.BleBeacon import BleBeacon
import pygatt

logging.basicConfig(
    level=logging.ERROR,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("beacon_connection.log"),
        logging.StreamHandler(sys.stdout)
    ]
)

class BLEConnectionHandler(AbstractBleConnectionHandler):
    beacon: BleBeacon

    def __init__(self, beacon: BleBeacon):
        self.beacon = beacon

    def request_data(self):
        adapter = pygatt.GATTToolBackend()

        try:
            adapter.start()
            device = adapter.connect(self.beacon.mac)
            value = device.char_read(self.beacon.uuid)

            return value

        except pygatt.exceptions.NotConnectedError as e:
            logging.error("No connection to "+self.beacon.mac)

        finally:
            adapter.stop()

