from abc import ABC, abstractmethod

from storage.BleBeacon import BleBeacon


class AbstractBleConnectionHandler(ABC):
    beacon: BleBeacon
    @abstractmethod
    def __init__(self, beacon: BleBeacon):
        pass

    @abstractmethod
    def request_data(self):
        pass