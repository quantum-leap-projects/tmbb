from abc import ABC, abstractmethod


class AbstractBleResponse(ABC):
    @abstractmethod
    def format_response(self, data):
        pass

