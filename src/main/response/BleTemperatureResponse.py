from response.AbstractBleResponse import AbstractBleResponse
from util.Ensure import Ensure


class BleTemperatureResponse(AbstractBleResponse):

    def format_response(self, data) -> dict:
        Ensure.not_none(data)
        Ensure.not_empty_list(data)
        # data element nr. 4 is irrelevant
        temp = None
        if data[0] == 0:  # positiv temperature
            temp = 1.0 * data[1] + 0.1 * data[2]  # combine temp decimal and floating point number
        if data[0] == 1:  # negativ temperature
            temp = (-1.0) * data[1] + 0.1 * data[2]  # combine temp decimal and floating point number

        humidity = 1.0 * data[4] + 0.1 * data[5]

        data: dict = {"temperature": temp, "humidity": humidity}
        return data
