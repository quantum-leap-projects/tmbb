from response.AbstractBleResponse import AbstractBleResponse


class BleBatteryResponse(AbstractBleResponse):

    def __init__(self, data):
        super().__init__(data)

    def format_response(self, data):
        pass