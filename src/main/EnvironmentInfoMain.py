import logging
import os
import sys
import time

from connection.JinouBleConnectionHandler import BLEConnectionHandler
from response.BleTemperatureResponse import BleTemperatureResponse
from server.BLERequest import BleRequest
from storage.BleBeacon import BleBeacon
from storage.BleConfig import BleConfig
from storage.file.FileWriter import FileWriter

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("main.log"),
        logging.StreamHandler(sys.stdout)
    ]
)


class EnvironmentInfoMain(object):
    log = logging.getLogger(__name__)
    __running: bool = False
    __config: dict = None
    __adapter: dict
    __response: dict
    __file_writer = FileWriter("data.csv")
    __timeout_in_s = 5

    def __init__(self):
        self.log.info("Init")
        path = os.path.dirname(__file__) + os.path.sep + "resource" + os.path.sep + "config.yml"
        self.log.info(path)
        ble_config = BleConfig()
        self.__config = ble_config.load_config(config_file=path)
        self.log.debug(self.__config)
        self.__running = True

    def is_running(self) -> bool:
        return self.__running

    def stop_run(self):
        self.__running = False

    def run(self):
        beacons = self.__gather_beacons()
        while self.is_running():
            for beacon in beacons:
                result = self.__read_beacon(beacon)
                self.__write_result(beacon, result)
                time.sleep(10)
            time.sleep(self.__config.get("time_frame").get("sec", 900))

    def __write_result(self, beacon:BleBeacon, result):
        self.log.debug("Writing Beacon: "+beacon.mac)
        if result is not None:
            self.__file_writer.append_file(beacon, result)

    def __read_beacon(self, beacon):
        response = BleTemperatureResponse()
        handler = BLEConnectionHandler(beacon)
        requester = BleRequest(handler, response)
        return requester.request()

    def __gather_beacons(self) -> list:
        beacons: list = []
        for address in self.__config.get('address'):
            for uuid in self.__config.get("uuid"):
                beacons.append(BleBeacon(self.__config.get('address').get(address), self.__config.get('uuid').get(uuid)))
        return beacons


if __name__ == '__main__':
    app = EnvironmentInfoMain()
    try:
        app.run()
    except KeyboardInterrupt:
        logging.info('Interrupted session, shutdown')
        app.stop_run()
        time.sleep(1)
        sys.exit(0)
