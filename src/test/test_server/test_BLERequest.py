from unittest import TestCase

from connection.AbstractBleConnectionHandler import AbstractBleConnectionHandler
from response.AbstractBleResponse import AbstractBleResponse
from response.BleTemperatureResponse import BleTemperatureResponse
from server.BLERequest import BleRequest
from storage.BleBeacon import BleBeacon
from test_server import mock_BleConnectionHandler
from util import EnsureException


class TestBLEAccessController(TestCase):
    def test_ctor__none_response__ensure_exception(self):
        handler: AbstractBleConnectionHandler = None
        response: AbstractBleResponse = BleTemperatureResponse()
        with self.assertRaises(EnsureException.NotNoneException):
            BleRequest(handler, response)

    def test_request__correct_request__returnCorrectData(self):
        address = "mac"
        uuid = "uuid"
        expected = {"temperature": 20, "humidity": 10}
        beacon: BleBeacon = BleBeacon(address, uuid)
        handler: AbstractBleConnectionHandler = mock_BleConnectionHandler.CorrectBleConnectionHandler(beacon)
        response: AbstractBleResponse = BleTemperatureResponse()
        under_test = BleRequest(handler, response)

        actual = under_test.request()

        self.assertEqual(expected, actual)


    def createNoneReturnBleHandler(self, beacon):
        handler: AbstractBleConnectionHandler = mock_BleConnectionHandler.NoneReturnBleConnectionHandler(beacon)
        return handler
