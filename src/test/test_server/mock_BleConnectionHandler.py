from connection.AbstractBleConnectionHandler import AbstractBleConnectionHandler
from storage.BleBeacon import BleBeacon


class NoneReturnBleConnectionHandler(AbstractBleConnectionHandler):

    def __init__(self, beacon: BleBeacon):
        pass

    def request_data(self):
        result: dict = None
        return


class CorrectBleConnectionHandler(AbstractBleConnectionHandler):

    def __init__(self, beacon: BleBeacon):
        pass

    def request_data(self):
        result = [0,20,0,0,10,0]
        return result
