from unittest import TestCase

from response import BleTemperatureResponse as Btr
from util import EnsureException


class TestBleTemperatureResponse(TestCase):
    def test_format_response__data_emtpy__ensure_exception(self):
        test_data = None
        undertest = Btr.BleTemperatureResponse()

        with self.assertRaises(EnsureException.NotNoneException):
            undertest.format_response(test_data)

    def test_format_response__data_empty_list__ensure_exception(self):
        test_data = []
        undertest = Btr.BleTemperatureResponse()

        with self.assertRaises(EnsureException.NotEmptyListException):
            undertest.format_response(test_data)

    def test_format_response__list_entry_for_positive_temperature_set__positive_temperature_returned(self):
        test_data = [0, 10, 0, 0, 50, 4]
        expected = {"temperature": 10, "humidity": 50.4}
        under_test = Btr.BleTemperatureResponse()

        actual = under_test.format_response(test_data)

        self.assertEqual(expected.get("temperature"), actual.get("temperature"))

    def test_format_response__list_entry_for_negative_temperature_set__negativ_temperature_returned(self):
        test_data = [1, 10, 0, 0, 50, 4]
        expected = {"temperature": -10, "humidity": 50.4}
        under_test = Btr.BleTemperatureResponse()

        actual = under_test.format_response(test_data)

        self.assertEqual(expected.get("temperature"), actual.get("temperature"))

    def test_format_response__list_entry_for_positive_temperature_set__correct_dict_returned(self):
        test_data = [0, 10, 0, 0, 50, 4]
        expected = {"temperature": 10, "humidity": 50.4}
        under_test = Btr.BleTemperatureResponse()

        actual = under_test.format_response(test_data)

        self.assertEqual(expected, actual)
