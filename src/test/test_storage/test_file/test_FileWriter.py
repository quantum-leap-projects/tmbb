from unittest import TestCase

from storage.file.FileWriter import FileWriter
from util import EnsureException


class test_FileWriter(TestCase):

    def test__append_file__empty_file_path__ensure_exception(self):
        file = ""

        with self.assertRaises(EnsureException.BlankStringException):
            FileWriter(file)
