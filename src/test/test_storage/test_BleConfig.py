import os
import unittest

from storage import IllConfigException
from storage.BleConfig import BleConfig


def create_config_dictionary():
    config = {'address': {'outdoor': "FF:40:AF:27:A1:A5", 'indoor': "FF:52:AF:27:BC:A5"},
              'uuid': {'temp': "0000aa21-0000-1000-8000-00805f9b34fb",
                       'battery': "0000ab21-0010-1000-8000-00805f9b34fb"}, 'time_frame': {'sec': 900}}

    return config


class test_BleConfig(unittest.TestCase):
    def test_load_config__empty_config__abort_when_no_config_was_found(self):
        path = "." + os.path.sep + "exampleConfigs" + os.path.sep + "empty_config.yml"
        underTest = BleConfig()

        with self.assertRaises(IllConfigException.EmptyConfigException):
            underTest.load_config(path)

    def test_load_config__no_addresses_config__abort_when_no_address_config_was_found(self):
        path = "." + os.path.sep + "exampleConfigs" + os.path.sep + "missing_address_config.yml"
        underTest = BleConfig()

        with self.assertRaises(IllConfigException.MissingBleAddressException):
            underTest.load_config(path)

    def test_load_config__no_uuid_config__abort_when_no_uuid_config_was_found(self):
        path = "." + os.path.sep + "exampleConfigs" + os.path.sep + "missing_uuid_config.yml"
        underTest = BleConfig()

        with self.assertRaises(IllConfigException.MissingUuidException):
            underTest.load_config(path)

    def test_load_config__correct_config__return_valid_config(self):
        path = "." + os.path.sep + "exampleConfigs" + os.path.sep + "example_config.yml"
        underTest = BleConfig()
        expected = create_config_dictionary()

        actual = underTest.load_config(path)

        self.assertEqual(expected, actual)
